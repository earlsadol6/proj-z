/**
 * Initialize AOS
 */

 const initializeAOS = () => {
    AOS.init({
      once: true,
      duration: 900,
      disable: 'mobile'
    });
  }
  
  export default initializeAOS