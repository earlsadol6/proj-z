/**
* Scroll Behavior
*/

const scrollBehavior = () => {

  var header = document.getElementById("main-header");
  window.addEventListener('scroll', ()=> {scrollFunction()})
  function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      if(header){
        header.style.display = "none";
      }
    } else {
      if(header){
        header.style.display = "block";
      }
    }
  }
}
export default scrollBehavior