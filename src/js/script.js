$(document).ready(function() {
    
    //activate wow.js
     new WOW().init();
  
    //activate fullpage.js
    $('#proj-z-fullpage').fullpage({
      scrollBar: true,
      navigation: true,
      navigationTooltips: ['WHAT WE DO', 'WHAT WE CAN OFFER', 'TECHNOLOGIES WE USE', 'OUR DEMOS', 'CONTACT US'],
      loopBottom: true,
      sectionSelector: 'section'
    });
  
  //apply color to each section from array
  int = -1;
  //color_array = ['#1abc9c','#c0392b','#9b59b6','#3498db','#f1c40f','#16a085'];

  $('section').each(function(){
    int++
    $(this).addClass('grid-item-' + int);
  });
  
});