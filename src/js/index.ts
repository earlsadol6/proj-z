import initializeAOS from './components/initializeAOS';
import scrollBehavior from './components/scrollBehavior';
import forEachPolyfill from './components/forEachPolyfill';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    initializeAOS()
    scrollBehavior()
    forEachPolyfill()
  },
  false
)
